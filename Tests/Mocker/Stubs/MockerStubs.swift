//
//  MockerSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 12/03/2020.
//  Copyright © 2020 JMiguel. All rights reserved.
//

@testable import AppFrameworkMocker

import XCTest

struct MockerStubs {

    static func httpRequest() {
        
        let uri = "https://mockserver.com/path?query=true"
        let headers = ["Content-Type": "application/json; charset=utf-8"]
        let body: [String:Any] = ["param1": "value1", "param2": ["value2a", "value2b"]]
        
        //-- stub matcher
        let matcher = Mocker.matcher(method: .post, uri: uri, body: body, headers: headers)
            
        //-- stub builder
        let builder = Mocker.builder(status: 200)
        
        //-- mock
        Mocker.httpMock(matcher: matcher, builder: builder)
    }

    static func jsonRequest() {
        
        let uri = "https://mockserver.com/path?query=true"
        var headers = ["Content-Type": "application/json; charset=utf-8"]
        var body: [String:Any] = ["param1": "value1", "param2": ["value2a", "value2b"]]
        
        //-- stub matcher
        let matcher = Mocker.matcher(method: .post, uri: uri, body: body, headers: headers)
            
        body = ["result": ["$type": "AuthChallenge",
                           "PasswordSalt": [84,68,5,219],
                           "Challenge": [205,79,3,44,41,195,23,240,74,56,211,202,81,246,201,19,33,73,34,168,6,47,17,123,145,1,98,1,253,190,132,177,166,121,184,1,178,100,160,10,80,44,197,26,223,59,105,119,113,190,211,159,245,116,12,148,229,70,98,251,111,116,100,7,81,24,196,151,224]],
                "error": NSNull(),
                "id": 0]
        
        guard let json = body.json else {
            XCTAssert(false, "Body is not valid!")
            return
        }
        
        headers = ["Content-Length": "\(json.count)", "Content-Type": "application/json; charset=utf-8"]
        
        //-- stub builder
        let builder = Mocker.builder(body: body, status: 200, headers: headers)
        
        //-- mock
        Mocker.jsonMock(matcher: matcher, builder: builder)
    }
}
