//
//  MockerSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 12/03/2020.
//  Copyright © 2020 JMiguel. All rights reserved.
//

@testable import AppFrameworkMocker

import AppFrameworkNetwork
import AppFrameworkCore
import XCTest

class MockerSpecs: MockTestCase {

    func testHTTPRequest() {
        
        //-- stub
        MockerStubs.httpRequest()
        
        //-- test
        
        let uri = "https://mockserver.com/path?query=true"
        let headers = ["Content-Type": "application/json; charset=utf-8"]
        let body: [String:Any] = ["param1": "value1", "param2": ["value2a", "value2b"]]
        
        let exp = self.expectation(description: "Starting Test")
        
        let request = DataRequest(url: URL(string: uri)!, headers: headers, body: body.json, success: { (data, response) in
            XCTAssertEqual(response?.statusCode ?? -1, 200)
            XCTAssertEqual(data.count, 0)
            
            exp.fulfill()
        })
        
        Network.add(request: request)
        
        self.waitForExpectations(timeout: self.timeout) { error in
            XCTAssertNil(error, "Test Failed | error: \(error?.localizedDescription ?? "none")")
        }
    }

    func testJSONRequest() {
        
        //-- stub
        MockerStubs.jsonRequest()
        
        //-- test
        
        let uri = "https://mockserver.com/path?query=true"
        let headers = ["Content-Type": "application/json; charset=utf-8"]
        let body: [String:Any] = ["param1": "value1", "param2": ["value2a", "value2b"]]
        
        let exp = self.expectation(description: "Starting Test")
        
        let request = JSONRequest(url: URL(string: uri)!, headers: headers, body: body.json, success: { (json, response) in
            let body: [String:Any] = ["result": ["$type": "AuthChallenge",
                                                 "PasswordSalt": [84,68,5,219],
                                                 "Challenge": [205,79,3,44,41,195,23,240,74,56,211,202,81,246,201,19,33,73,34,168,6,47,17,123,145,1,98,1,253,190,132,177,166,121,184,1,178,100,160,10,80,44,197,26,223,59,105,119,113,190,211,159,245,116,12,148,229,70,98,251,111,116,100,7,81,24,196,151,224]],
                                      "error": NSNull(),
                                      "id": 0]
            
            let headers = ["Content-Length": "\(body.json?.count ?? 0)", "Content-Type": "application/json; charset=utf-8"]

            guard let rheaders = response?.allHeaderFields as? [String:String] else {
                XCTAssert(false, "Test Failed!")
                return
            }
            
            //-- headers
            for header in headers.keys {
                let header = rheaders.first { $0.key == header && $0.value == headers[header] }
                guard header != nil else {
                    XCTAssert(false, "Test Failed!")
                    return
                }
            }
            
            //-- body
            XCTAssertEqual(json.first?.json, body.json)
            exp.fulfill()
        })
        
        Network.add(request: request)
        
        self.waitForExpectations(timeout: self.timeout) { error in
            XCTAssertNil(error, "Test Failed | error: \(error?.localizedDescription ?? "none")")
        }
    }
}
