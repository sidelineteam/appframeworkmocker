// swift-tools-version:5.3

//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-ios13.0-simulator"
//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-macos10.14"

import PackageDescription

let package = Package(name: "AppFrameworkMocker",
                      
    platforms: [.iOS(.v13), .macOS(.v10_12)], //, .tvOS(.v10), .watchOS(.v3)],

    products: [
        .library(name: "AppFrameworkMocker", targets: ["AppFrameworkMocker"])
    ],

    dependencies: [

      //.package(name: "AppFrameworkCore", path: "../core"),
        .package(name: "AppFrameworkCore", url: "https://bitbucket.org/sidelineteam/appframeworkcore", .branch("main")),
        
      //.package(name: "AppFrameworkNetwork", path: "../network"),
        .package(name: "AppFrameworkNetwork", url: "https://bitbucket.org/sidelineteam/appframeworknetwork", .branch("main")),

        //-- Mocker
        .package(url: "https://github.com/AliSoftware/OHHTTPStubs", from: "9.0.0")
    ],

    targets: [
        .target(name: "AppFrameworkMocker", dependencies: ["AppFrameworkCore", .product(name: "OHHTTPStubsSwift", package: "OHHTTPStubs")], path: "Sources"),

        //-- test target
        .testTarget(name: "AppFrameworkTests", dependencies: ["AppFrameworkMocker", "AppFrameworkNetwork"], path: "Tests")
    ],

    swiftLanguageVersions: [.v5]
)
