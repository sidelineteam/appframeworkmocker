# AppFrameworkV2
AppFrameworkV2 is a framework for those who want to start coding their ideas as soon as possible, without having to care about the repetitive code that is required when creating a new project. It is a Swift, more modern version of the AppFramework, written in ObjC.

## How to Get Started

Create the app project including AppFrameworkV2 using `Swift Package Manager`.

Since this framework has a lot of features, sometimes including only a subset might be handy. With that in mind, AppFrameworkV2 is devided in feature targets, each one providing smaller sets of features.

## Installation

### Swift Package Manager

`Swift Package Manager` is the official package manager from Apple. In order to use 'AppFrameworkV2' through it, use the provided tools on XCode. The packages available are:

```swift
AppFrameworkCore
AppFrameworkMocker              #-- includes AppFrameworkCore
AppFrameworkNetwork             #-- includes AppFrameworkCore
AppFrameworkDataStore           #-- includes AppFrameworkCore
```

### Tests

AppFrameworkV2 tests are available on the SPM package.

## Usage

```Swift
// swift-tools-version:5.3

//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-ios13.0-simulator"

import PackageDescription

let package = Package(name: "MyApp",
    platforms: [.iOS(.v11), .macOS(.v10_12)],

    products: [
        .library(name: "MyApp", targets: ["MyApp"]),
        .library(name: "MyAppStubs", targets: ["MyAppStubs"])
    ],

    dependencies: [
        //-- AppFramework
        .package(name: "AppFrameworkDataStore", path: "../../appframeworkv2/datastore"),
        .package(name: "AppFrameworkNetwork", path: "../../appframeworkv2/network"),
        .package(name: "AppFrameworkMocker", path: "../../appframeworkv2/mocker"),
        .package(name: "AppFrameworkCore", path: "../../appframeworkv2/core"),
    ],

    targets: [
        .target(name: "MyApp", dependencies: ["AppFrameworkDataStore", "AppFrameworkNetwork"], path: "Sources"),
        .target(name: "MyAppStubs", dependencies: ["MyApp", "AppFrameworkMocker"], path: "Stubs"),

        .testTarget(name: "MyAppTests", dependencies: ["MyAppStubs"], path: "Tests")
    ],

    swiftLanguageVersions: [.v5]
)
```

### AFMocker

#### Architecture

* `Mocker`
  * `Matcher`
  * `Builder`
  * `Mock`
* `MockerTestCase`

##### Mocker

Mocker provides an interface for the creation of tests that require network activity, by stubing it. This is composed by a `Matcher`, a `Builder` and a `Mock`

###### Usage

```swift
struct MockerStubs {

    static func jsonRequest() {

        let uri = "https://mockserver.com/path?query=true"
        var headers = ["Content-Type": "application/json; charset=utf-8"]
        var body: [String:Any] = ["param1": "value1", "param2": ["value2a", "value2b"]]

        //-- stub matcher
        let matcher = Mocker.matcher(method: .post, uri: uri, body: body, headers: headers)

        body = ["result": ["$type": "AuthChallenge",
                           "PasswordSalt": [84,68,5,219],
                           "Challenge": [205,79,3,44]],
                "error": NSNull(),
                "id": 0]

        guard let json = body.json else {
            XCTAssert(false, "Body is not valid!")
            return
        }

        headers = ["Content-Length": "\(json.count)", "Content-Type": "application/json; charset=utf-8"]

        //-- stub builder
        let builder = Mocker.builder(body: body, status: 200, headers: headers)

        //-- mock
        Mocker.jsonMock(matcher: matcher, builder: builder)
    }
}
```

First of all, we should create the `Mock`. For that create a function that details the information being sent to the network, by creating a `Matcher`, and the information being expected on the response from the network, represented by the `Builder`. After having these objects, define the mock that best suits the desired scenario.

```swift
class MockerSpecs: MockTestCase {

  func testJSONRequest() {

        //-- stub
        MockerStubs.jsonRequest()

        //-- test

        let uri = "https://mockserver.com/path?query=true"
        let headers = ["Content-Type": "application/json; charset=utf-8"]
        let body: [String:Any] = ["param1": "value1", "param2": ["value2a", "value2b"]]

        let exp = self.expectation(description: "Starting Test")

        let request = JSONRequest(url: URL(string: uri)!, headers: headers, body: body.json, success: { (json, response) in
            let body: [String:Any] = ["result": ["PasswordSalt": [84,68,5,219],
                                                 "Challenge": [205,79,3,44]],
                                      "error": NSNull(),
                                      "id": 0]

            let headers = ["Content-Length": "\(body.json?.count ?? 0)", "Content-Type": "application/json; charset=utf-8"]

            guard let rheaders = response?.allHeaderFields as? [String:String] else {
                XCTAssert(false, "Test Failed!")
                return
            }

            //-- headers
            for header in headers.keys {
                let header = rheaders.first { $0.key == header && $0.value == headers[header] }
                guard header != nil else {
                    XCTAssert(false, "Test Failed!")
                    return
                }
            }

            //-- body
            XCTAssertEqual(json.first?.json, body.json)
            exp.fulfill()
        })

        Network.add(request: request)

        self.waitForExpectations(timeout: self.timeout) { error in
            XCTAssertNil(error, "Test Failed | error: \(error?.localizedDescription ?? "none")")
        }
    }
}
```

To create the test, first define the required mock, followed by the code that you want to test. The network calls on your code will be stubbed by the mock.

###### Methods & Vars

```swift
public static func matcher(method: HTTPMethod, uri: String, body: Any? = nil, headers: [String:String]? = nil) -> Matcher
```

Creates a matcher for the mock. It represents the info being sent on the request.

```swift
public static func builder(body: Any? = nil, status: Int, headers: [String:Any]? = nil) -> Builder
```

Creates a builder for the mock. It represents the info being received on the response.

```swift
public static func httpMock(matcher: Matcher, delay: TimeInterval = 0.0, builder: Builder)
```

Creates the mock that stubs an HTTP response. It validates any request being done against the matcher. and in case of match, issues the response to it, based on the builder's info.

```swift
public static func jsonMock(matcher: Matcher, delay: TimeInterval = 0.0, builder: Builder)
```

Creates the mock that stubs a JSON response. It validates any request being done against the matcher. and in case of match, issues the response to it, based on the builder's info.

---

## Dependencies

On this version of AppFramework I choose to use 3rd party libs as much as possible... and justifiable. The libs listed below are the core of some of the AppFrameworkV2's features.

```
FirebaseRemoteConfig #-- Remote configurations  (Core)

Umbrella             #-- Analytics              (Core)

Umbrella/Firebase    #-- Analytics              (Core)

SwiftyStoreKit       #-- InAppPurchases         (Core)

Realm                #-- Storage                (DataStore)
```

Adding to these, some other libs where also used on AppFrameworkV2 to provide internal features to the AppFrameworkV2, hence not having been wrapped by AppFrameworkV2.

```
RNCryptor         #-- Encryption                                 (Core)

DeviceKit         #-- Device info                                (Core)

ReachabilitySwift #-- Network Reachability                       (Network)
```

---

## Author

Jorge Miguel

## License

AppFrameworkV2 is available under the MIT license. See the LICENSE file for more info.
