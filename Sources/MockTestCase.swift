//
//  MockTestCase.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 08/06/2020.
//  Copyright © 2020 JMiguel. All rights reserved.
//

import XCTest
import OHHTTPStubs

open class MockTestCase : XCTestCase {
    
    public let timeout: TimeInterval = 10
    
    override open func setUp() {
        super.setUp()
        
        
    }
    
    override open func tearDown() {
        super.tearDown()
        HTTPStubs.removeAllStubs()
    }
}
