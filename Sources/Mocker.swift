//
//  Mocker.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 03/06/2020.
//  Copyright © 2020 JMiguel. All rights reserved.
//

import XCTest
import OHHTTPStubs
import AppFrameworkCore
import OHHTTPStubsSwift

public enum MockerError: Error {
    case missingBody
    case unknownTypeBody
}

public struct Mocker {
    
    private static var shared = Mocker()
    
    private var logs = [String]()
    
    //--
    
    public enum HTTPMethod : String {
        case post = "POST"
        case get  = "GET"
    }
    
    public struct Matcher {
        
        public let method: HTTPMethod
        
        public let uri: String
        public let body: Any?
        public let headers: [String:String]?
         
        public init(method: HTTPMethod, uri: String, body: Any?, headers: [String:String]? = nil) {
            self.uri = uri
            self.body = body
            self.method = method
            self.headers = headers
        }
    }
    
    public struct Builder {
        public let status: Int
        public let body: Any?
        public let headers: [String:Any]?
        
        public init(body: Any? = nil, status: Int, headers: [String:Any]? = nil) {
            self.body = body
            self.status = status
            self.headers = headers
        }
    }
    
    //--
    
    private static func setup() {
        HTTPStubs.onStubMissing { _ in
            DispatchQueue.syncOnMain {
                self.shared.logs.forEach { Log.notice("Mocker.Logs", $0) }
                self.shared.logs.removeAll()
            }
        }
        
        HTTPStubs.onStubActivation { _, _, _ in
            DispatchQueue.syncOnMain {
                self.shared.logs.forEach { Log.notice("Mocker.Logs", $0) }
                self.shared.logs.removeAll()
            }
        }
    }
    
    public static func matcher(method: HTTPMethod, uri: String, body: Any? = nil, headers: [String:String]? = nil) -> Matcher {
        return Matcher(method: method, uri: uri, body: body, headers: headers)
    }
    
    public static func builder(body: Any? = nil, status: Int, headers: [String:Any]? = nil) -> Builder {
        return Builder(body: body, status: status, headers: headers)
    }
    
    //-- MARK: Mockers
    
    public static func jsonMock(matcher: Matcher, delay: TimeInterval = 0.0, builder: Builder) {
        stub(condition: { request in
            validateMatcher(matcher, request: request, requestBody: request.ohhttpStubs_httpBody)
            
        }, response: { _ in
            guard let body = builder.body else {
                Log.error(ClassInfo.methodName(self), "Builder's body is missing.")
                return HTTPStubsResponse(error: MockerError.missingBody)
            }
            
            if let body = builder.body as? String {
                return HTTPStubsResponse(data: body.data(using: .utf8)!, statusCode: Int32(builder.status), headers: builder.headers)
            }
            
            return HTTPStubsResponse(jsonObject: body, statusCode: Int32(builder.status), headers: builder.headers)
        })
    }
    
    public static func httpMock(matcher: Matcher, delay: TimeInterval = 0.0, builder: Builder) {
        stub(condition: { request in
            validateMatcher(matcher, request: request, requestBody: request.ohhttpStubs_httpBody)
            
        }, response: { _ in
            if let body = builder.body as? String {
                return HTTPStubsResponse(data: body.data(using: .utf8)!, statusCode: Int32(builder.status), headers: builder.headers)
            }
            if let body = builder.body as? Data {
                return HTTPStubsResponse(data: body, statusCode: Int32(builder.status), headers: builder.headers)
            }
            if builder.body != nil {
                Log.warning(ClassInfo.methodName(self), "Builder's body is of an unknown type.")
                return HTTPStubsResponse(error: MockerError.unknownTypeBody)
            }
            
            return HTTPStubsResponse(data: Data(), statusCode: Int32(builder.status), headers: builder.headers)
        })
    }
    
    //-- MARK: validation
    
    fileprivate static func validateMatcher(_ matcher: Matcher, request: URLRequest, requestBody: Data?) -> Bool {
        
        DispatchQueue.once {
            setup()
        }
        
        self.shared.logs.removeAll()
        
        //-- url
        guard self.uriValidation(matcher.uri, request: request) else {
            self.shared.logs.append("🔴 URI do not match! \(matcher.uri) - \(request.url?.absoluteString ?? "nil")")
            self.shared.logs.append("🟡 The request is not stubbed...")
            return false
        }
        
        //-- http method
        guard request.httpMethod ?? "unknown" == matcher.method.rawValue else {
            self.shared.logs.append("🔴 request method do not match! \(matcher.method) - \(request.httpMethod ?? "nil")")
            self.shared.logs.append("🟡 The request is not stubbed...")
            return false
        }
        
        //-- headers
        guard self.headersValidation(matcher.headers ?? [:], request: request) else {
            self.shared.logs.append("🔴 headers do not match! \(matcher.headers?.description ?? "nil") - \(request.allHTTPHeaderFields?.description ?? "nil")")
            self.shared.logs.append("🟡 The request is not stubbed...")
            return false
        }
        
        self.shared.logs.append("🟢 URI match: \(matcher.uri)")
        self.shared.logs.append("🟢 request method match: \(matcher.method)")
        
        if let headers = matcher.headers {
            self.shared.logs.append("🟢 headers match: \(headers.description)")
        }
        
        var success = false
        
        //-- bodies are nil?
        if requestBody == nil || matcher.body == nil {
            success = requestBody?.count ?? 0 == 0 && matcher.body == nil
            
        } else if let body = matcher.body as? String {
            success = Mocker.jsonBodyValidation(body, requestBody: requestBody!)
            
        } else if let bodyObj = matcher.body as? [Any], let body = bodyObj.json {
            success = Mocker.jsonBodyValidation(body, requestBody: requestBody!)
            
        } else if let bodyObj = matcher.body as? [String:Any], let body = bodyObj.json {
            success = Mocker.jsonBodyValidation(body, requestBody: requestBody!)
            
        } else if let data = matcher.body as? Data, let body = data.utf8String {
            success = Mocker.jsonBodyValidation(body, requestBody: requestBody!)
            
        } else {
            XCTAssert(false, "Unknown body format: \(matcher.body ?? "nil")")
        }
        
        if success {
            if requestBody != nil {
                self.shared.logs.append("🟢 body match: \(String(reflecting: requestBody!.utf8String!))".replacingOccurrences(of: "\\\"", with: "\""))
            }
            self.shared.logs.append("🟢 this request is stubbed 🎉")
            
        } else {
            self.shared.logs.append("🔴 mock body does not match: \(String(reflecting: requestBody!.utf8String!))".replacingOccurrences(of: "\\\"", with: "\""))
            self.shared.logs.append("🟡 this request is not stubbed 🤔")
        }
        
        return success
    }
    
    //-- MARK: validators
    
    fileprivate static func uriValidation(_ uri: String, request: URLRequest) -> Bool {
       
        //-- uri
        if let urlString = request.url?.absoluteString {
            return urlString == uri
        }
        
        return false
    }
    
    fileprivate static func headersValidation(_ headers: [String:String], request: URLRequest) -> Bool {
        if headers.count > 0 {
            guard let rHeaders = request.allHTTPHeaderFields else {
                return false
            }
            
            for header in headers.keys {
                let header = rHeaders.first { $0.key == header && $0.value == headers[header] }
                guard header != nil else {
                    return true
                }
            }
        }
        return true
    }
    
    fileprivate static func jsonBodyValidation(_ body: String, requestBody: Data) -> Bool {
        
        //-- test body
        guard let bodyData = body.data(using: .utf8),
            var bodyObj = try? JSONSerialization.jsonObject(with: bodyData) as? [String: Any] else {
            self.shared.logs.append("🔴 cannot parse stub body!")
                return false
        }
        
        //-- lets ignore the request id
        bodyObj.removeValue(forKey: "id")
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: bodyObj, options: .sortedKeys),
            let jsonString = String(data: jsonData, encoding: .utf8) else {
            self.shared.logs.append("🔴 cannot encode stub body!")
                return false
        }
        
        //-- request body
        guard var reqBodyObj = try? JSONSerialization.jsonObject(with: requestBody) as? [String: Any] else {
            self.shared.logs.append("🔴 cannot parse request body!")
            return false
        }
        
        //-- lets ignore the request id
        reqBodyObj.removeValue(forKey: "id")
        
        guard let reqJsonData = try? JSONSerialization.data(withJSONObject: reqBodyObj, options: .sortedKeys),
            let reqJsonString = String(data: reqJsonData, encoding: .utf8) else {
            self.shared.logs.append("🔴 cannot encode request body!")
                return false
        }
        
        return (reqJsonString == jsonString)
    }
}
